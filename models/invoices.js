const Sequelize = require('sequelize');
var db = require('../config/db');

module.exports = db.define('invoice', {
    invoiceId: {
        type: Sequelize.STRING,
        primaryKey: true,
        unique: true
    },
    numberPlate: {
        type: Sequelize.STRING
    },
    taskId: {
        type: Sequelize.STRING
    },
    hiredBy: {
        type: Sequelize.STRING
    },
    invoiceTitle: {
        type: Sequelize.STRING
    },
    lpo: {
        type: Sequelize.STRING
    },
    invoiceDescription: {
        type: Sequelize.STRING(1000)
    },
    workingHours: {
        type: Sequelize.DECIMAL(32, 1),
        defaultValue: 0
    },
    interval: {
        type: Sequelize.DECIMAL(32, 1),
        defaultValue: 0
    },
    intervalType: {
        type: Sequelize.STRING
    },
    workingRate: {
        type: Sequelize.INTEGER
    },
    total: {
        type: Sequelize.DECIMAL(32, 2),
        defaultValue: 0
    },
    income: {
        type: Sequelize.DECIMAL(32, 2),
        defaultValue: 0
    },
    discount: {
        type: Sequelize.DECIMAL(32, 2),
        defaultValue: 0
    },
    totalExpenses: {
        type: Sequelize.DECIMAL(32, 2),
        defaultValue: 0
    },
    createdBy: {
        type: Sequelize.STRING
    },
    markedAsReceivedBy: {
        type: Sequelize.STRING
    },
    paymentMethod: {
        type: Sequelize.STRING
    },
    paymentDescription: {
        type: Sequelize.STRING(1000)
    },
    chequeNumber: {
        type: Sequelize.STRING
    },
    invoiceDocuments: {
        type: Sequelize.JSONB
    },
    withDiesel: {
        type: Sequelize.STRING,
        defaultValue: 'NO'
    },
    includeExpenses: {
        type: Sequelize.STRING,
        defaultValue: 'NO'
    },
    invoiceDate_str: {
        type: Sequelize.STRING,
    },
    status: {
        type: Sequelize.STRING,
        defaultValue: 'PENDING'
    },
    companyId: {
        type: Sequelize.STRING
    },
    companyName: {
        type: Sequelize.STRING(1000)
    },
    invoiceDate: {
        type: Sequelize.DATE
    }
});