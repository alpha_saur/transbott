const Sequelize = require('sequelize');
var db = require('../config/db');

module.exports = db.define('task', {
    taskId: {
        type: Sequelize.STRING,
        primaryKey: true,
        unique: true
    },
    numberPlate: {
        type: Sequelize.STRING
    },
    hiredBy: {
        type: Sequelize.STRING
    },
    taskTitle: {
        type: Sequelize.STRING
    },
    lpo: {
        type: Sequelize.STRING
    },
    taskDescription: {
        type: Sequelize.STRING(1000)
    },
    workingHours: {
        type: Sequelize.DECIMAL(32, 1),
        defaultValue: 0
    },
    remainingHours: {
        type: Sequelize.DECIMAL(32, 1),
        defaultValue: 0
    },
    interval: {
        type: Sequelize.DECIMAL(32, 1),
        defaultValue: 0
    },
    intervalType: {
        type: Sequelize.STRING
    },
    workingRate: {
        type: Sequelize.INTEGER
    },
    total: {
        type: Sequelize.DECIMAL(32, 2),
        defaultValue: 0
    },
    income: {
        type: Sequelize.DECIMAL(32, 2),
        defaultValue: 0
    },
    discount: {
        type: Sequelize.DECIMAL(32, 2),
        defaultValue: 0
    },
    totalExpenses: {
        type: Sequelize.DECIMAL(32, 2),
        defaultValue: 0
    },
    createdBy: {
        type: Sequelize.STRING
    },
    markedAsReceivedBy: {
        type: Sequelize.STRING
    },
    paymentMethod: {
        type: Sequelize.STRING
    },
    paymentDescription: {
        type: Sequelize.STRING(1000)
    },
    chequeNumber: {
        type: Sequelize.STRING
    },
    taskDocuments: {
        type: Sequelize.JSONB
    },
    withDiesel: {
        type: Sequelize.STRING,
        defaultValue: 'NO'
    },
    startDate: {
        type: Sequelize.STRING,
    },
    startTime: {
        type: Sequelize.STRING,
    },
    status: {
        type: Sequelize.STRING,
        defaultValue: 'PENDING'
    },
    companyId: {
        type: Sequelize.STRING
    },
    companyName: {
        type: Sequelize.STRING(1000)
    },
    taskDate: {
        type: Sequelize.DATE
    },
    endDate: {
        type: Sequelize.DATE
    },
    nextInvoiceStartDate: {
        type: Sequelize.DATE
    }
});