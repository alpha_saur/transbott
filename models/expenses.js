const Sequelize = require('sequelize');
var db = require('../config/db');

module.exports = db.define('expense', {
    expenseId: {
        type: Sequelize.STRING,
        primaryKey: true,
        unique: true
    },
    taskId: {
        type: Sequelize.STRING
    },
    invoiceId: {
        type: Sequelize.STRING
    },
    numberPlate: {
        type: Sequelize.STRING
    },
    expenseTitle: {
        type: Sequelize.STRING
    },
    expenseDescription: {
        type: Sequelize.STRING(1000)
    },
    recieveable: {
        type: Sequelize.STRING(10)
    },
    expenses: {
        type: Sequelize.DECIMAL(32, 2)
    },
    status: {
        type: Sequelize.STRING,
        defaultValue: 'PENDING'
    },
    createdBy: {
        type: Sequelize.STRING
    }
});