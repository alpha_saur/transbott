const Sequelize = require('sequelize');
var db = require('../config/db');

module.exports = db.define('user', {
    userId: {
        type: Sequelize.STRING,
        primaryKey: true,
        unique: true
    },
    companyId: {
        type: Sequelize.STRING
    },
    companyName: {
        type: Sequelize.STRING(1000)
    },
    userName: {
        type: Sequelize.STRING,
        unique: true
    },
    userType: {
        type: Sequelize.STRING
    },
    firstName: {
        type: Sequelize.STRING
    },
    lastName: {
        type: Sequelize.STRING
    },
    mobile: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    },
    passwordSalt: {
        type: Sequelize.STRING
    }
});