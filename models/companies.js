const Sequelize = require('sequelize');
var db = require('../config/db');

module.exports = db.define('company', {
    companyId: {
        type: Sequelize.STRING,
        primaryKey: true,
        unique: true
    },
    companyName: {
        type: Sequelize.STRING(1000)
    },
    address: {
        type: Sequelize.STRING
    },
    contact: {
        type: Sequelize.STRING
    },
    logo: {
        type: Sequelize.STRING
    },
    active: {
        type: Sequelize.STRING,
        defaultValue: 'NO'
    },
    documentsRequired: {
        type: Sequelize.STRING,
        defaultValue: 'YES'
    }
});