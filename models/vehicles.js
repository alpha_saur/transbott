const Sequelize = require('sequelize');
var db = require('../config/db');

module.exports = db.define('vehicle', {
    numberPlate: {
        type: Sequelize.STRING,
         primaryKey: true,
        unique: true
    },
    vehicleType: {
        type: Sequelize.STRING
    },
    ownerType: {
        type: Sequelize.STRING
    },
    owner: {
        type: Sequelize.STRING
    },
    status: {
        type: Sequelize.STRING,
        defaultValue: 'AVAILABLE'
    },
    companyId: {
        type: Sequelize.STRING
    },
    mulkia: {
        type: Sequelize.STRING
    },
    mulkiaExpiry: {
        type: Sequelize.STRING
    },
    companyName: {
        type: Sequelize.STRING(1000)
    }
});