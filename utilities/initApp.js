var Users = require('../models/users');
var Companies = require('../models/companies');
var random = require('./random');
var crypto = require('crypto');
Companies.findOne({
    where: {
        companyId: 'firstalmathal2345678yghbnrtcfvgfty'
    }
})
    .then(function (companyFromRepo) {

        if (!companyFromRepo) {
            Companies.create({
                companyId: 'firstalmathal2345678yghbnrtcfvgfty',
                companyName: 'Al Amthal',
                address: 'flat-209 B-Block Falcon Towers, Ajman',
                contact: "067476991",
                logo: 'alamthal.png'
            })
                .then(function () {
                    Users.findOne({
                        where: {
                            userName: 'admin'
                        }
                    })
                        .then(function (userFromRepo) {

                            if (!userFromRepo) {
                                try {
                                    var salt = random.randomLongstr();
                                    var password = crypto.createHmac('sha256', salt)
                                        .update('asdf@123')
                                        .digest('hex');

                                    Users.create({
                                        companyId: 'firstalmathal2345678yghbnrtcfvgfty',
                                        companyName: 'Al Amthal',
                                        userId: 'sedfghbnj89876trtyuiuguhjhjnjk',
                                        userName: 'admin',
                                        userType: 'admin',
                                        firstName: "Administrator",
                                        lastName: '',
                                        mobile: '+971502072912',
                                        email: 'chandcheeema@gmail.com',
                                        password: password,
                                        passwordSalt: salt
                                    })
                                        .then(function () {
                                            console.log('admin user created')
                                        })
                                        .catch(function (err) {
                                            console.log(err)
                                        });
                                } catch (err) {
                                    console.log(err)
                                }
                            }
                        })
                        .catch(function (err) {
                            console.log(err)
                        });
                })
                .catch(function (err) {
                    console.log(err)
                });
        }
    })
    .catch(function (err) {
        console.log(err)
    });