const Companies = require('../models/companies');
const Users = require('../models/users');
const crypto = require('crypto');
const random = require('../utilities/random');
const utils = require('../utilities/utils');

module.exports = function (app) {


  app.get('/api/users/:userId', function (req, res, next) {
    Users.findOne({
      where: {
        userId: req.params.userId
      },
      attributes: {
        exclude: ['password', 'passwordSalt']
      }
    })
      .then(function (userFromRepo) {
        if (userFromRepo) {
          res.json(userFromRepo);
          res.end();
        }
        else {
          res.statusCode = 400;
          res.setHeader('response-description', 'Invalid user id, User not found');
          res.end();
        }
      })
      .catch(function () {
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9211655');
        res.end();
      });
  });

  app.get('/api/users', function (req, res, next) {
    Users.findAll({
      where: {},
      attributes: {
        exclude: ['password', 'passwordSalt']
      }
    })
      .then(function (usersFromRepo) {
        res.json(usersFromRepo);
        res.end();
      })
      .catch(function () {
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9211650');
        res.end();
      });
  });

  app.post('/api/users', function (req, res, next) {
    utils.velidateRequiredKeys(req.body,
      [
        { key: 'userName', name: 'User name' },
        { key: 'password', name: 'Password' },
        { key: 'mobile', name: 'Mobile number' },
        { key: 'userType', name: 'User type' },
      ],
      function (errorField) {
        if (!errorField) {

          Users.findOne({
            where: {
              userName: req.body.userName.toLowerCase()
            }
          })
            .then(function (userFromRepo) {

              if (!userFromRepo) {
                try {

                  var salt = random.randomLongstr();
                  req.body.password = crypto.createHmac('sha256', salt)
                    .update(req.body.password)
                    .digest('hex');

                  Users.create({
                    userId: random.randomstr(),
                    userName: req.body.userName.toLowerCase(),
                    userType: req.body.userType.toLowerCase(),
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    mobile: req.body.mobile,
                    email: req.body.email,
                    password: req.body.password,
                    passwordSalt: salt
                  })
                    .then(function () {
                      res.statusCode = 201;
                      res.end();
                    })
                    .catch(function () {
                      res.statusCode = 500;
                      res.setHeader('response-description', 'Oops, Something went wrong ER9211538');
                      res.end();
                    });
                } catch (err) {
                  res.statusCode = 500;
                  res.setHeader('response-description', 'Oops, Something went wrong ER9211558');
                  res.end();
                }
              }
              else {
                res.statusCode = 403;
                res.setHeader('response-description', 'User name already exists');
                res.end();
              }
            })
            .catch(function (err) {
              console.log(err)
              res.statusCode = 500;
              res.setHeader('response-description', 'Oops, Something went wrong ER9211639');
              res.end();
            });


        }
        else {
          res.statusCode = 400;
          res.setHeader('response-description', errorField + ' is required');
          res.end();
        }
      })
  });

  app.post('/api/login', function (req, res, next) {
    utils.velidateRequiredKeys(req.body,
      [
        { key: 'userName', name: 'User name' },
        { key: 'password', name: 'Password' }
      ],
      function (errorField) {
        if (!errorField) {
          try {

            Users.findOne({
              where: {
                userName: req.body.userName
              }
            })
              .then(function (userFromRepo) {
                if (userFromRepo) {
                  Companies.findOne({
                    where: {
                      companyId: userFromRepo.companyId,
                      active: 'YES'
                    }
                  })
                    .then(function (companyFromRepo) {
                      if (companyFromRepo) {
                        var salt = userFromRepo.passwordSalt;
                        req.body.password = crypto.createHmac('sha256', salt)
                          .update(req.body.password)
                          .digest('hex');

                        if (req.body.password == userFromRepo.password) {
                          res.statusCode = 202;
                          res.json(companyFromRepo);
                          res.end();
                        }
                        else {
                          res.statusCode = 401;
                          res.setHeader('response-description', 'Invalid user name or password');
                          res.end();
                        }
                      }
                      else {
                        res.statusCode = 401;
                        res.setHeader('response-description', 'Invalid user name or password');
                        res.end();
                      }
                    })
                    .catch(function (err) {
                      console.log(err);
                      res.statusCode = 500;
                      res.setHeader('response-description', 'Oops, Something went wrong ER9211633');
                      res.end();
                    });
                }
                else {
                  res.statusCode = 401;
                  res.setHeader('response-description', 'Invalid user name or password');
                  res.end();
                }
              })
              .catch(function (err) {
                console.log(err);
                res.statusCode = 500;
                res.setHeader('response-description', 'Oops, Something went wrong ER9211633');
                res.end();
              });

          } catch (err) {
            console.log(err);
            res.statusCode = 500;
            res.setHeader('response-description', 'Oops, Something went wrong ER9211634');
            res.end();
          }
        }
        else {
          res.statusCode = 400;
          res.setHeader('response-description', errorField + ' is required');
          res.end();
        }
      })
  });

  app.put('/api/users', function (req, res, next) {
    utils.velidateRequiredKeys(req.body,
      [
        { key: 'userId', name: 'User id' },
        { key: 'userName', name: 'User name' },
        { key: 'email', name: 'Email' },
        { key: 'mobile', name: 'Mobile number' },
        { key: 'userType', name: 'User type' },
      ],
      function (errorField) {
        if (!errorField) {

          Users.findOne({
            where: {
              userName: req.body.userName.toLowerCase(),
              userId: req.body.userId
            }
          })
            .then(function (userFromRepo) {

              if (userFromRepo) {
                try {

                  userFromRepo.userType = req.body.userType.toLowerCase();
                  userFromRepo.firstName = req.body.firstName;
                  userFromRepo.lastName = req.body.lastName;
                  userFromRepo.mobile = req.body.mobile;
                  userFromRepo.email = req.body.email;

                  userFromRepo.save()
                    .then(function () {
                      res.statusCode = 200;
                      res.end();
                    })
                    .catch(function () {
                      res.statusCode = 500;
                      res.setHeader('response-description', 'Oops, Something went wrong ER9211705');
                      res.end();
                    });

                } catch (err) {
                  console.log(err)
                  res.statusCode = 500;
                  res.setHeader('response-description', 'Oops, Something went wrong ER9211706');
                  res.end();
                }
              }
              else {
                res.statusCode = 400;
                res.setHeader('response-description', 'User not found');
                res.end();
              }
            })
            .catch(function (err) {
              console.log(err)
              res.statusCode = 500;
              res.setHeader('response-description', 'Oops, Something went wrong ER9211707');
              res.end();
            });


        }
        else {
          res.statusCode = 400;
          res.setHeader('response-description', errorField + ' is required');
          res.end();
        }
      })
  });



}
