const Tasks = require('../models/tasks');
const Invoices = require('../models/invoices');
const Expenses = require('../models/expenses');
const crypto = require('crypto');
const random = require('../utilities/random');
const utils = require('../utilities/utils');
const gatekeeper = require('../middlewares/gatekeeper');

module.exports = function (app) {

  app.get('/api/expenses/:taskId', gatekeeper.authenticateUser, function (req, res, next) {

    Expenses.findAll({
      where: {
        taskId: req.params.taskId
      }
    })
      .then(function (expensesFromRepo) {
        res.json(expensesFromRepo);
        res.end();
      })
      .catch(function () {
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
        res.end();
      });
  });

  app.post('/api/expenses', gatekeeper.authenticateUser, function (req, res, next) {
    utils.velidateRequiredKeys(req.body,
      [
        { key: 'taskId', name: 'Task id' },
        { key: 'expenseTitle', name: 'Expense title' },
        { key: 'expenses', name: 'Expenses' }
      ],
      function (errorField) {
        if (!errorField) {
          Tasks.findOne({
            where: {
              taskId: req.body.taskId
            }
          })
            .then(function (taskFromRepo) {
              Expenses.create({
                expenseId: random.randomstr(),
                taskId: taskFromRepo.taskId,
                numberPlate: taskFromRepo.numberPlate.toUpperCase(),
                recieveable: req.body.recieveable,
                expenseTitle: req.body.expenseTitle,
                expenseDescription: req.body.expenseDescription,
                expenses: req.body.expenses,
                createdBy: req.userName
              })
                .then(function () {

                  if (req.body.recieveable == 'NO') {
                    taskFromRepo.totalExpenses = Number(taskFromRepo.totalExpenses) + Number(req.body.expenses);
                  }
                  else {
                    taskFromRepo.totalExpenses = Number(taskFromRepo.totalExpenses) - Number(req.body.expenses);
                  }
                  taskFromRepo.income = Number(taskFromRepo.total) - Number(taskFromRepo.totalExpenses) - Number(taskFromRepo.discount);
                  taskFromRepo.save()
                    .then(function () {
                      res.statusCode = 201;
                      res.end();
                    })
                    .catch(function (err) {
                      console.log(err)
                      res.statusCode = 500;
                      res.setHeader('response-description', 'Oops, Something went wrong ER92101081');
                      res.end();
                    });

                })
                .catch(function () {
                  res.statusCode = 500;
                  res.setHeader('response-description', 'Oops, Something went wrong ER9210108');
                  res.end();
                });
            })
            .catch(function (err) {
              console.log(err)
              res.statusCode = 500;
              res.setHeader('response-description', 'Oops, Something went wrong ER9210108');
              res.end();
            });
        }
        else {
          res.statusCode = 400;
          res.setHeader('response-description', errorField + ' is required');
          res.end();
        }
      })
  });

  app.post('/api/expenses/invoices', gatekeeper.authenticateUser, function (req, res, next) {
    utils.velidateRequiredKeys(req.body,
      [
        { key: 'taskId', name: 'Task id' },
        { key: 'invoiceId', name: 'Invoice id' },
        { key: 'expenseTitle', name: 'Expense title' },
        { key: 'expenses', name: 'Expenses' }
      ],
      function (errorField) {
        if (!errorField) {
          Tasks.findOne({
            where: {
              taskId: req.body.taskId
            }
          })
            .then(function (taskFromRepo) {
              Invoices.findOne({
                where: {
                  taskId: req.body.taskId,
                  invoiceId: req.body.invoiceId
                }
              })
                .then(function (invoiceFromRepo) {
                  Expenses.create({
                    expenseId: random.randomstr(),
                    taskId: taskFromRepo.taskId,
                    invoiceId: invoiceFromRepo.invoiceId,
                    numberPlate: taskFromRepo.numberPlate.toUpperCase(),
                    recieveable: req.body.recieveable,
                    expenseTitle: req.body.expenseTitle,
                    expenseDescription: req.body.expenseDescription,
                    expenses: req.body.expenses,
                    createdBy: req.userName
                  })
                    .then(function () {

                      if (req.body.recieveable == 'NO') {
                        taskFromRepo.totalExpenses = Number(taskFromRepo.totalExpenses) + Number(req.body.expenses);
                      }
                      else {
                        taskFromRepo.totalExpenses = Number(taskFromRepo.totalExpenses) - Number(req.body.expenses);
                      }

                      invoiceFromRepo.totalExpenses = Number(invoiceFromRepo.totalExpenses) + Number(req.body.expenses);

                      taskFromRepo.income = Number(taskFromRepo.total) - Number(taskFromRepo.totalExpenses) - Number(taskFromRepo.discount);
                      invoiceFromRepo.income = Number(invoiceFromRepo.total) + Number(invoiceFromRepo.totalExpenses) - Number(invoiceFromRepo.discount);
                      taskFromRepo.save()
                        .then(function () {
                          invoiceFromRepo.save()
                            .then(function () {
                              res.statusCode = 201;
                              res.end();
                            })
                            .catch(function (err) {
                              console.log(err)
                              res.statusCode = 500;
                              res.setHeader('response-description', 'Oops, Something went wrong ER92101081');
                              res.end();
                            });
                        })
                        .catch(function (err) {
                          console.log(err)
                          res.statusCode = 500;
                          res.setHeader('response-description', 'Oops, Something went wrong ER92101081');
                          res.end();
                        });

                    })
                    .catch(function () {
                      res.statusCode = 500;
                      res.setHeader('response-description', 'Oops, Something went wrong ER9210108');
                      res.end();
                    });
                })
                .catch(function (err) {
                  console.log(err)
                  res.statusCode = 500;
                  res.setHeader('response-description', 'Oops, Something went wrong ER9210108');
                  res.end();
                });
            })
            .catch(function (err) {
              console.log(err)
              res.statusCode = 500;
              res.setHeader('response-description', 'Oops, Something went wrong ER9210108');
              res.end();
            });
        }
        else {
          res.statusCode = 400;
          res.setHeader('response-description', errorField + ' is required');
          res.end();
        }
      })
  });

}
