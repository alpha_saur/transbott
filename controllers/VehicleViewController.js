const crypto = require('crypto');
const random = require('../utilities/random');
const utils = require('../utilities/utils');
const gatekeeper = require('../middlewares/gatekeeper');
const apiHelpers = require('../utilities/apiHelpers');

module.exports = function (app) {

  app.get('/vehicles', gatekeeper.authenticateSession, function (req, res, next) {
    apiHelpers.genericAPIHelperWithAuth(req, '/api/vehicles', 'GET',
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 200) {
          res.render('vehicles/vehicles',
            {
              error: req.flash('error')[0],
              success: req.flash('success')[0],
              vehicles: apiRepo.response,
              session: req.session
            });
        }
        else {
          req.flash('error', apiHelpers.description);
          req.session.save(function () {
            res.render('vehicles/vehicles', {
              error: req.flash('error')[0],
              success: req.flash('success')[0],
              session: req.session
            });
          });
        }
      });
  });

  app.post('/vehicles', gatekeeper.authenticateSession, function (req, res, next) {
    if (req.body.vehicleType == 'other') {
      req.body.vehicleType = req.body.vehicleTypeCustom;
    }
    if (req.body.ownerType == 'company') {
      req.body.owner = req.session.company.companyName;
    }

    apiHelpers.genericAPIHelperWithAuth(req, '/api/vehicles', 'POST',
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 201) {
          req.flash('success', 'Vehicle successfully added');
          req.session.save(function () {
            res.redirect('/vehicles');
          });
        }
        else {
          req.flash('error', apiRepo.description);
          req.session.save(function () {
            res.redirect('/vehicles');
          });
        }
      });

  });

  app.post('/vehicles/update', gatekeeper.authenticateSession, function (req, res, next) {
    if (req.body.vehicleType == 'other') {
      req.body.vehicleType = req.body.vehicleTypeCustom;
    }
    if (req.body.ownerType == 'company') {
      req.body.owner = req.session.company.companyName;
    }

    apiHelpers.genericAPIHelperWithAuth(req, '/api/vehicles', 'PUT',
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 200) {
          req.flash('success', 'Vehicle successfully updated');
          req.session.save(function () {
            res.redirect('/vehicles');
          });
        }
        else {
          req.flash('error', apiRepo.description);
          req.session.save(function () {
            res.redirect('/vehicles');
          });
        }
      });

  });


}
