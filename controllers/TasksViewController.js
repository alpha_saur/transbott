const crypto = require('crypto');
const random = require('../utilities/random');
const utils = require('../utilities/utils');
const gatekeeper = require('../middlewares/gatekeeper');
const apiHelpers = require('../utilities/apiHelpers');
const async = require('async');

module.exports = function (app) {


  app.get('/tasks', gatekeeper.authenticateSession, function (req, res, next) {
    var tasks = [];
    var vehicles = [];
    async.parallel([
      function (callback) {
        apiHelpers.genericAPIHelperWithAuth(req, '/api/vehicles/available', 'GET',
          req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
            if (apiRepo.statusCode == 200) {
              vehicles = apiRepo.response;
              callback();
            }
            else {
              callback(apiHelpers.description);
            }
          });
      }
    ], function (err) {
      if (!err) {
        res.render('tasks/tasks-json',
          {
            error: req.flash('error')[0],
            success: req.flash('success')[0],
            vehicles: vehicles,
            session: req.session
          });
      }
      else {
        req.flash('error', err);
        req.session.save(function () {
          res.redirect('/');
        });
      }
    });
  });

  app.get('/tasks/:numberPlate', gatekeeper.authenticateSession, function (req, res, next) {
    var vehicle = {};
    async.parallel([
      function (callback) {
        apiHelpers.genericAPIHelperWithAuth(req, '/api/vehicles/' + req.params.numberPlate, 'GET',
          req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
            if (apiRepo.statusCode == 200) {
              vehicle = apiRepo.response;
              callback();
            }
            else {
              callback(apiHelpers.description);
            }
          });
      }
    ], function (err) {
      if (!err) {
        res.render('tasks/vehicle-tasks-json',
          {
            error: req.flash('error')[0],
            success: req.flash('success')[0],
            numberPlate: req.params.numberPlate,
            vehicle: vehicle,
            session: req.session
          });
      }
      else {
        req.flash('error', err);
        req.session.save(function () {
          res.redirect('/');
        });
      }
    });
  });

  app.get('/notbeingused/tasks', gatekeeper.authenticateSession, function (req, res, next) {
    var tasks = [];
    var vehicles = [];
    async.parallel([
      function (callback) {
        apiHelpers.genericAPIHelperWithAuth(req, '/api/tasks/', 'GET',
          req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
            if (apiRepo.statusCode == 200) {
              tasks = apiRepo.response;
              callback();
            }
            else {
              callback(apiHelpers.description);
            }
          });
      },
      function (callback) {
        apiHelpers.genericAPIHelperWithAuth(req, '/api/vehicles/available', 'GET',
          req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
            if (apiRepo.statusCode == 200) {
              vehicles = apiRepo.response;
              callback();
            }
            else {
              callback(apiHelpers.description);
            }
          });
      }
    ], function (err) {
      if (!err) {
        res.render('tasks/tasks',
          {
            error: req.flash('error')[0],
            success: req.flash('success')[0],
            tasks: tasks,
            vehicles: vehicles,
            session: req.session
          });
      }
      else {
        req.flash('error', err);
        req.session.save(function () {
          res.redirect('/');
        });
      }
    });
  });

  app.get('/notbeingused/tasks/:numberPlate', gatekeeper.authenticateSession, function (req, res, next) {
    var tasks = [];
    var vehicle = {};
    async.parallel([
      function (callback) {
        apiHelpers.genericAPIHelperWithAuth(req, '/api/vehicles/' + req.params.numberPlate, 'GET',
          req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
            if (apiRepo.statusCode == 200) {
              vehicle = apiRepo.response;
              callback();
            }
            else {
              callback(apiHelpers.description);
            }
          });
      },
      function (callback) {
        apiHelpers.genericAPIHelperWithAuth(req, '/api/tasks/' + req.params.numberPlate, 'GET',
          req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
            if (apiRepo.statusCode == 200) {
              tasks = apiRepo.response;
              callback();
            }
            else {
              callback(apiHelpers.description);
            }
          });
      }
    ], function (err) {
      if (!err) {
        res.render('tasks/vehicle-tasks',
          {
            error: req.flash('error')[0],
            success: req.flash('success')[0],
            numberPlate: req.params.numberPlate,
            tasks: tasks,
            vehicle: vehicle,
            session: req.session
          });
      }
      else {
        req.flash('error', err);
        req.session.save(function () {
          res.redirect('/');
        });
      }
    });
  });

  app.get('/tasks/:numberPlate/:taskId', gatekeeper.authenticateSession, function (req, res, next) {
    var task = {};
    var vehicle = {};
    var expenses = [];
    async.parallel([
      function (callback) {
        apiHelpers.genericAPIHelperWithAuth(req, '/api/tasks/details/' + req.params.taskId, 'GET',
          req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
            if (apiRepo.statusCode == 200) {
              task = apiRepo.response;
              callback();
            }
            else {
              callback(apiHelpers.description);
            }
          });
      },
      function (callback) {
        apiHelpers.genericAPIHelperWithAuth(req, '/api/expenses/' + req.params.taskId, 'GET',
          req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
            if (apiRepo.statusCode == 200) {
              expenses = apiRepo.response;
              callback();
            }
            else {
              callback(apiHelpers.description);
            }
          });
      },
      function (callback) {
        apiHelpers.genericAPIHelperWithAuth(req, '/api/vehicles/' + req.params.numberPlate, 'GET',
          req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
            if (apiRepo.statusCode == 200) {
              vehicle = apiRepo.response;
              callback();
            }
            else {
              callback(apiHelpers.description);
            }
          });
      }
    ], function (err) {
      if (!err) {
        res.render('tasks/task-details',
          {
            error: req.flash('error')[0],
            success: req.flash('success')[0],
            task: task,
            expenses: expenses,
            vehicle: vehicle,
            session: req.session
          });
      }
      else {
        req.flash('error', err);
        req.session.save(function () {
          res.redirect('/tasks/' + req.param.numberPlate + '/' + req.params.taskId);
        });
      }
    });
  });

  app.post('/tasks/addhours', gatekeeper.authenticateSession, function (req, res, next) {

    apiHelpers.genericAPIHelperWithAuth(req, '/api/tasks/addhours/', 'PUT',
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 200) {
          task = apiRepo.response;
          req.flash('success', 'Hours successfully added');
          req.session.save(function () {
            res.redirect('/tasks/' + req.body.numberPlate + '/' + req.body.taskId);
          });
        }
        else {
          req.flash('error', apiRepo.description);
          req.session.save(function () {
            res.redirect('/tasks/' + req.body.numberPlate + '/' + req.body.taskId);
          });
        }
      });
  });

  app.post('/tasks', gatekeeper.authenticateSession, function (req, res, next) {

    apiHelpers.genericAPIHelperWithAuth(req, '/api/tasks', 'POST',
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 201) {
          req.flash('success', 'Task successfully created');
          req.session.save(function () {
            res.redirect('/tasks/' + req.body.numberPlate);
          });
        }
        else {
          req.flash('error', apiRepo.description);
          req.session.save(function () {
            res.redirect('/tasks/' + req.body.numberPlate);
          });
        }
      });

  });

  app.post('/tasks/received', gatekeeper.authenticateSession, function (req, res, next) {
    apiHelpers.genericAPIMultiPartHelperWithAuth(req, '/api/tasks/received', 'PUT',
      ['taskPhoto'],
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 200) {
          req.flash('success', 'Task successfully marked as received');
          req.session.save(function () {
            res.redirect('/tasks/' + req.body.numberPlate + '/' + req.body.taskId);
          });
        }
        else {
          req.flash('error', apiRepo.description);
          req.session.save(function () {
            res.redirect('/tasks/' + req.body.numberPlate + '/' + req.body.taskId);
          });
        }
      });

  });

  app.post('/tasks/complete', gatekeeper.authenticateSession, function (req, res, next) {
    apiHelpers.genericAPIMultiPartHelperWithAuth(req, '/api/tasks/complete', 'PUT',
      ['timesheetPhoto'],
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 200) {
          req.flash('success', 'Task successfully marked as complete');
          req.session.save(function () {
            res.redirect('/tasks/' + req.body.numberPlate + '/' + req.body.taskId);
          });
        }
        else {
          req.flash('error', apiRepo.description);
          req.session.save(function () {
            res.redirect('/tasks/' + req.body.numberPlate + '/' + req.body.taskId);
          });
        }
      });

  });

  app.post('/tasks/lpo', gatekeeper.authenticateSession, function (req, res, next) {
    apiHelpers.genericAPIMultiPartHelperWithAuth(req, '/api/tasks/lpo', 'PUT',
      ['lpoPhoto'],
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 200) {
          req.flash('success', 'LPO successfully attached');
          req.session.save(function () {
            res.redirect('/tasks/' + req.body.numberPlate + '/' + req.body.taskId);
          });
        }
        else {
          req.flash('error', apiRepo.description);
          req.session.save(function () {
            res.redirect('/tasks/' + req.body.numberPlate + '/' + req.body.taskId);
          });
        }
      });

  });

  app.get('/json/tasks/data', gatekeeper.authenticateSession, function (req, res, next) {
    apiHelpers.genericAPIHelperWithAuth(req, '/api/json/tasks?search=' + encodeURIComponent(req.query.search) + '&offset=' + encodeURIComponent(req.query.offset) + '&limit=' + encodeURIComponent(req.query.limit), 'GET',
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 200) {
          res.json({ rows: apiRepo.response.rows, total: apiRepo.response.total });
          res.end();
        }
        else {
          req.flash('error', apiRepo.description);
          res.json({ rows: [], total: 0 });
          res.end();
        }
      });

  });

  app.get('/json/tasks/data/:numberPlate', gatekeeper.authenticateSession, function (req, res, next) {
    apiHelpers.genericAPIHelperWithAuth(req, '/api/json/tasks/' + req.params.numberPlate + '?search=' + encodeURIComponent(req.query.search) + '&offset=' + encodeURIComponent(req.query.offset) + '&limit=' + encodeURIComponent(req.query.limit), 'GET',
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 200) {
          res.json({ rows: apiRepo.response.rows, total: apiRepo.response.total });
          res.end();
        }
        else {
          req.flash('error', apiRepo.description);
          res.json({ rows: [], total: 0 });
          res.end();
        }
      });

  });
}
