const Tasks = require('../models/tasks');
const Expenses = require('../models/expenses');
const crypto = require('crypto');
const fs = require('fs');
const random = require('../utilities/random');
const utils = require('../utilities/utils');
const gatekeeper = require('../middlewares/gatekeeper');

module.exports = function (app) {

  app.get('/api/downloads/tasks/:taskId/:fileName', function (req, res, next) {

    Tasks.findOne({
      where: {
        taskId: req.params.taskId
      }
    })
      .then(function (taskFromRepo) {
        if (taskFromRepo) {
          fs.readFile(__dirname + '/../utilities/files/tasks/' + req.params.taskId + '/' + req.params.fileName, function (err, data) {

            res.writeHead(200, { 'Content-Type': 'image/jpeg' });
            res.end(data); // Send the file data to the browser.

          });
        }
      })
      .catch(function () {
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
        res.end();
      });
  });

  app.get('/api/downloads/companies/logos/:fileName', function (req, res, next) {

    fs.readFile(__dirname + '/../utilities/logos/' + '/' + req.params.fileName, function (err, data) {

      res.writeHead(200, { 'Content-Type': 'image/png' });
      res.end(data);

    });

  });

  app.get('/api/downloads/tasks/:taskId/invoices/:invoiceId/:fileName', function (req, res, next) {

    Tasks.findOne({
      where: {
        taskId: req.params.taskId
      }
    })
      .then(function (taskFromRepo) {
        if (taskFromRepo) {
          fs.readFile(__dirname + '/../utilities/files/tasks/' + req.params.taskId + '/' + req.params.invoiceId + '/' + req.params.fileName, function (err, data) {

            res.writeHead(200, { 'Content-Type': 'image/jpeg' });
            res.end(data); // Send the file data to the browser.

          });
        }
      })
      .catch(function () {
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
        res.end();
      });
  });

}
