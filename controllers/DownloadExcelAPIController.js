const Tasks = require('../models/tasks');
const Invoices = require('../models/invoices');
const Expenses = require('../models/expenses');
const crypto = require('crypto');
const json2xls = require('json2xls');
const fs = require('fs');
const random = require('../utilities/random');
const utils = require('../utilities/utils');
const gatekeeper = require('../middlewares/gatekeeper');

module.exports = function (app) {
  app.use(json2xls.middleware);
  app.get('/api/downloads/excels/tasks/:companyId', function (req, res, next) {
    var startDate = new Date;
    var endDate = new Date;
    if (req.query.startDate && req.query.endDate) {
      endDate = new Date(req.query.endDate);
      startDate = new Date(req.query.startDate);
    }

    Tasks.findAll({
      where: {
        companyId: req.params.companyId,
        taskDate: {
          $between: [startDate, endDate]
        },
      },
      attributes: { exclude: ['companyId'] }
    })
      .then(function (expensesFromRepo) {
        var json = [];
        json = JSON.parse(JSON.stringify(expensesFromRepo));
        for (var i = 0; i < json.length; i++) {
          json[i].discount = JSON.stringify(json[i].discount);
          json[i].total = JSON.stringify(json[i].total);
          json[i].totalExpenses = JSON.stringify(json[i].totalExpenses);
          json[i].income = JSON.stringify(json[i].income);
        }
        try {
          var xls = json2xls(json);
          res.xls('data.xlsx', json);
        }
        catch (err) {
          res.end();
        }
      })
      .catch(function () {
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
        res.end();
      });
  });

  app.get('/api/downloads/excels/invoices/:companyId', function (req, res, next) {
    var startDate = new Date;
    var endDate = new Date;
    if (req.query.startDate && req.query.endDate) {
      endDate = new Date(req.query.endDate);
      startDate = new Date(req.query.startDate);
    }

    Invoices.findAll({
      where: {
        companyId: req.params.companyId,
        invoiceDate: {
          $between: [startDate, endDate]
        },
      },
      attributes: { exclude: ['companyId','includeExpenses','invoiceDate'] }
    })
      .then(function (expensesFromRepo) {
        var json = [];
        json = JSON.parse(JSON.stringify(expensesFromRepo));
        for (var i = 0; i < json.length; i++) {
          json[i].discount = JSON.stringify(json[i].discount);
          json[i].total = JSON.stringify(json[i].total);
          json[i].totalExpenses = JSON.stringify(json[i].totalExpenses);
          json[i].receiveables = JSON.stringify(json[i].income);
        delete json[i].income;
        }
        try {
          var xls = json2xls(json);
          res.xls('data.xlsx', json);
        }
        catch (err) {
          console.log(err)
          res.end();
        }
      })
      .catch(function (err) {
        console.log(err)
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
        res.end();
      });
  });

  app.get('/api/downloads/excels/tasks/:companyId/:numberPlate', function (req, res, next) {

    var startDate = new Date;
    var endDate = new Date;
    if (req.query.startDate && req.query.endDate) {
      endDate = new Date(req.query.endDate);
      startDate = new Date(req.query.startDate);
    }
    Tasks.findAll({
      where: {
        companyId: req.params.companyId,
        numberPlate: req.params.numberPlate,
        taskDate: {
          $between: [startDate, endDate]
        }
      },
      attributes: { exclude: ['companyId'] }
    })
      .then(function (expensesFromRepo) {

        var json = [];
        json = JSON.parse(JSON.stringify(expensesFromRepo));
        for (var i = 0; i < json.length; i++) {
          json[i].discount = JSON.stringify(json[i].discount);
          json[i].total = JSON.stringify(json[i].total);
          json[i].totalExpenses = JSON.stringify(json[i].totalExpenses);
          json[i].income = JSON.stringify(json[i].income);
        }
        try {
          var xls = json2xls(json);
          // fs.writeFileSync('data.xlsx', xls, 'binary');
          res.xls('data.xlsx', json);
        }
        catch (err) {
          res.end();
        }
      })
      .catch(function (err) {
        console.log(err)
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
        res.end();
      });
  });

}
