const crypto = require('crypto');
const random = require('../utilities/random');
const utils = require('../utilities/utils');
const gatekeeper = require('../middlewares/gatekeeper');
const apiHelpers = require('../utilities/apiHelpers');
const async = require('async');

module.exports = function (app) {

  app.get('/dashboard', gatekeeper.authenticateSession, function (req, res, next) {
    var barData = {};
    var summary = {};
    async.parallel([
      function (callback) {
        apiHelpers.genericAPIHelperWithAuth(req, '/api/dashboard/invoices', 'GET',
          req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
            if (apiRepo.statusCode == 200) {
              barData = apiRepo.response;
              callback();
            }
            else {
              callback(apiHelpers.description);
            }
          });
      },
      function (callback) {
        apiHelpers.genericAPIHelperWithAuth(req, '/api/dashboard/summary', 'GET',
          req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
            if (apiRepo.statusCode == 200) {
              summary = apiRepo.response;
              callback();
            }
            else {
              callback(apiHelpers.description);
            }
          });
      }
    ], function (err) {
      if (!err) {
        res.render('dashboard/dashboard',
          {
            summary: summary,
            barData: barData,
            error: req.flash('error')[0],
            success: req.flash('success')[0],
            session: req.session
          });
      }
      else {
        res.render('500');
      }
    });
  });

}
