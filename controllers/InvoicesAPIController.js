const Invoices = require('../models/invoices');
const Tasks = require('../models/tasks');
const Vehicle = require('../models/vehicles');
const crypto = require('crypto');
const random = require('../utilities/random');
const utils = require('../utilities/utils');
const config = require('../config/config');
var fs = require('fs');
const gatekeeper = require('../middlewares/gatekeeper');
const Expenses = require('../models/expenses');
const async = require('async');

module.exports = function (app) {

  app.get('/api/invoices/:taskId', gatekeeper.authenticateUser, function (req, res, next) {
    Invoices.findAll({
      where: {
        taskId: req.params.taskId,
        companyId: req.companyId
      },
      order: '"createdAt" DESC'
    })
      .then(function (invoiceFromRepo) {
        res.json(invoiceFromRepo);
        res.end();
      })
      .catch(function () {
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
        res.end();
      });
  });

  app.get('/api/invoices/details/:invoiceId', gatekeeper.authenticateUser, function (req, res, next) {

    var vehicle = {};
    var expenses = [];
    Invoices.findOne({
      where: {
        invoiceId: req.params.invoiceId,
        companyId: req.companyId
      }
    })
      .then(function (invoiceFromRepo) {
        async.parallel([
          function (callback) {
            Vehicle.findOne({
              where: {
                numberPlate: invoiceFromRepo.numberPlate,
                companyId: req.companyId
              }
            })
              .then(function (vehicleFromRepo) {
                vehicle = vehicleFromRepo;
                callback();
              })
              .catch(function (err) {
                console.log(err);
                callback('Oops, Something went wrong ER92101001');
              });
          },
          function (callback) {
            Expenses.findAll({
              where: {
                taskId: invoiceFromRepo.taskId,
                invoiceId: invoiceFromRepo.invoiceId,
                recieveable: 'YES'
              }
            })
              .then(function (expensesFromRepo) {
                expenses = expensesFromRepo;
                callback();
              })
              .catch(function (err) {
                console.log(err);
                callback('Oops, Something went wrong ER921010012');
              });
          }
        ], function (err) {
          if (err) {
            res.statusCode = 500;
            res.setHeader('response-description', err);
            res.end();
          }
          else {
            res.json({ vehicle: vehicle, invoice: invoiceFromRepo, expenses: expenses });
          }
        });
      })
      .catch(function (err) {
        console.log(err)
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
        res.end();
      });
  });

  app.post('/api/invoices', gatekeeper.authenticateUser, function (req, res, next) {
    if (req.documentsRequired == 'YES' && !req.files.timesheetPhoto) {
      res.statusCode = 400;
      res.setHeader('response-description', 'Time sheet copy is required');
      res.end();
    }
    else {
      utils.velidateRequiredKeys(req.body,
        [
          { key: 'workingHours', name: 'Number of hours' },
        ],
        function (errorField) {
          if (!errorField) {

            Tasks.findOne({
              where: {
                taskId: req.body.taskId,
                companyId: req.companyId
              }
            })
              .then(function (taskFromRepo) {
                if (taskFromRepo) {
                  var newInvoice = JSON.parse(JSON.stringify(taskFromRepo));
                  newInvoice.workingHours = Number(req.body.workingHours);

                  if (taskFromRepo.remainingHours < newInvoice.workingHours) {
                    newInvoice.workingHours = taskFromRepo.remainingHours;
                  }

                  newInvoice.total = newInvoice.workingHours * Number(taskFromRepo.workingRate);
                  newInvoice.income = newInvoice.total;
                  newInvoice.invoiceId = 5 + '' + random.random10Digits();
                  newInvoice.invoiceTitle = req.body.invoiceTitle;
                  newInvoice.invoiceDescription = req.body.invoiceDescription;
                  newInvoice.discount = req.body.discount ? Number(req.body.discount) : 0;
                  newInvoice.totalExpenses = 0;
                  newInvoice.createdBy = req.userName;
                  newInvoice.invoiceDate = new Date;
                  newInvoice.invoiceDate_str = utils.formatDatetoMMddYYYY(newInvoice.invoiceDate);


                  taskFromRepo.discount = Number(taskFromRepo.discount) + newInvoice.discount;
                  taskFromRepo.income = Number(taskFromRepo.total) - Number(taskFromRepo.totalExpenses) - taskFromRepo.discount;

                  newInvoice.income = newInvoice.total - newInvoice.discount;

                  utils.saveFileAt('/files/tasks/' + req.body.taskId + '/' + newInvoice.invoiceId, req.files.timesheetPhoto, function (err, fileName) {
                    if (err && err != 'nofile') {
                      console.log(err);
                      res.statusCode = 500;
                      res.setHeader('response-description', 'Oops, Something went wrong ER92123501');
                      res.end();
                    }
                    else {
                      var docs = [];
                      if (req.files.timesheetPhoto) {
                        req.files.timesheetPhoto.fileType = 'timesheet';
                        req.files.timesheetPhoto.fileName = fileName;
                        req.files.timesheetPhoto.uploadedBy = req.userName;
                        docs.push(req.files.timesheetPhoto)

                        newInvoice.invoiceDocuments = docs;
                      }

                      Invoices.create(newInvoice)
                        .then(function () {
                          taskFromRepo.remainingHours = taskFromRepo.remainingHours - newInvoice.workingHours;
                          taskFromRepo.save()
                            .then(function () {
                              res.json(newInvoice.invoiceId);
                            })
                            .catch(function (err) {
                              console.log(err);
                              res.statusCode = 500;
                              res.setHeader('response-description', 'Oops, Something went wrong ER92101081');
                              res.end();
                            });
                        })
                        .catch(function (err) {
                          console.log(err);
                          res.statusCode = 500;
                          res.setHeader('response-description', 'Oops, Something went wrong ER921010822');
                          res.end();
                        });

                    }
                  });
                }
                else {
                  res.statusCode = 400;
                  res.setHeader('response-description', 'Task not found');
                  res.end();
                }
              })
              .catch(function (err) {
                console.log(err);
                res.statusCode = 500;
                res.setHeader('response-description', 'Oops, Something went wrong ER92101083');
                res.end();
              });
          }
          else {
            res.statusCode = 400;
            res.setHeader('response-description', errorField + ' is required');
            res.end();
          }
        })
    }
  });

  app.put('/api/invoices/received', gatekeeper.authenticateUser, function (req, res, next) {
    utils.velidateRequiredKeys(req.body,
      [
        { key: 'paymentMethod', name: 'Payment method' }
      ],
      function (errorField) {
        if (!errorField) {
          if (req.body.paymentMethod.toUpperCase() == 'CHEQUE' && !req.body.chequeNumber) {
            res.statusCode = 400;
            res.setHeader('response-description', 'Cheque number is required');
            res.end();
          }
          else {

            Invoices.findOne({
              where: {
                invoiceId: req.body.invoiceId,
                status: 'PENDING',
                companyId: req.companyId
              }
            })
              .then(function (invoiceFromRepo) {

                if (invoiceFromRepo) {
                  invoiceFromRepo.markedAsReceivedBy = req.userName;
                  invoiceFromRepo.paymentMethod = req.body.paymentMethod;
                  invoiceFromRepo.paymentDescription = req.body.paymentDescription;
                  var newDiscount = req.body.discount ? Number(req.body.discount) : 0;
                  invoiceFromRepo.discount = Number(invoiceFromRepo.discount) + newDiscount;
                  invoiceFromRepo.income = invoiceFromRepo.total - Number(invoiceFromRepo.totalExpenses) - invoiceFromRepo.discount;

                  if (invoiceFromRepo.paymentMethod == 'CHEQUE') {
                    invoiceFromRepo.chequeNumber = req.body.chequeNumber;
                  }
                  invoiceFromRepo.status = 'RECEIVED';

                  Tasks.findOne({
                    where: {
                      taskId: invoiceFromRepo.taskId,
                      companyId: req.companyId
                    }
                  })
                    .then(function (taskFromRepo) {

                      if (taskFromRepo) {
                        taskFromRepo.discount = Number(taskFromRepo.discount) + newDiscount;
                        taskFromRepo.income = Number(taskFromRepo.total) - Number(taskFromRepo.totalExpenses) - taskFromRepo.discount;

                        invoiceFromRepo.save()
                          .then(function () {

                            taskFromRepo.save()
                              .then(function () {

                                res.statusCode = 200;
                                res.end();

                              })
                              .catch(function (err) {
                                console.log(err)
                                res.statusCode = 500;
                                res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
                                res.end();
                              });

                          })
                          .catch(function (err) {
                            console.log(err)
                            res.statusCode = 500;
                            res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
                            res.end();
                          });

                      }
                      else {
                        res.statusCode = 400;
                        res.setHeader('response-description', 'Task not found');
                        res.end();
                      }
                    })
                    .catch(function (err) {
                      console.log(err)
                      res.statusCode = 500;
                      res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
                      res.end();
                    });

                }
                else {
                  res.statusCode = 400;
                  res.setHeader('response-description', 'Invoice not found');
                  res.end();
                }
              })
              .catch(function (err) {
                console.log(err)
                res.statusCode = 500;
                res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
                res.end();
              });
          }
        }
        else {
          res.statusCode = 400;
          res.setHeader('response-description', errorField + ' is required');
          res.end();
        }
      })
  });


  app.get('/api/json/invoices', gatekeeper.authenticateUser, function (req, res, next) {

    var limit = 10;
    var offset = 0;
    var search = '';

    if (req.query.limit != null && req.query.limit != 'undefined') {
      limit = req.query.limit;
    }

    if (req.query.offset != null && req.query.offset != 'undefined') {
      offset = req.query.offset;
    }

    if (req.query.search && req.query.search != 'undefined') {
      search = req.query.search;
    }



    var searchnumber;

    searchnumber = Number(search)

    if (!searchnumber) {
      searchnumber = null
    }

    Invoices.count({
      where: {
        companyId: req.companyId,
        $or: [
          { status: { $iLike: '%' + search + '%' } },
          { invoiceTitle: { $iLike: '%' + search + '%' } },
          { invoiceId: { $iLike: '%' + search + '%' } },
          { workingHours: searchnumber },
          { workingRate: searchnumber },
          { income: searchnumber },
          { invoiceDate_str: { $iLike: '%' + search + '%' } },
          { numberPlate: { $iLike: '%' + search + '%' } },
        ]
      }
    })
      .then(function (totalCount) {
        Invoices.findAll({
          where: {
            companyId: req.companyId,
            $or: [
              { status: { $iLike: '%' + search + '%' } },
              { invoiceTitle: { $iLike: '%' + search + '%' } },
              { invoiceId: { $iLike: '%' + search + '%' } },
              { workingHours: searchnumber },
              { workingRate: searchnumber },
              { income: searchnumber },
              { invoiceDate_str: { $iLike: '%' + search + '%' } },
              { numberPlate: { $iLike: '%' + search + '%' } },
            ]
          },
          order: '"status" ASC',
          offset: offset,
          limit: limit
        })
          .then(function (invoiceFromRepo) {
            res.json({ rows: invoiceFromRepo, total: totalCount });
            res.end();
          })
          .catch(function (err) {
            console.log(err);
            res.statusCode = 500;
            res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
            res.end();
          });
      })
      .catch(function (err) {
        console.log(err);
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
        res.end();
      });
  });

}