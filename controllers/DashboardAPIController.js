const Invoices = require('../models/invoices');
const Tasks = require('../models/tasks');
const Vehicles = require('../models/vehicles');
const crypto = require('crypto');
const random = require('../utilities/random');
const utils = require('../utilities/utils');
const config = require('../config/config');
var fs = require('fs');
const gatekeeper = require('../middlewares/gatekeeper');
const Expenses = require('../models/expenses');
const async = require('async');

module.exports = function (app) {

  app.get('/api/dashboard/invoices', gatekeeper.authenticateUser, function (req, res, next) {
    var barData = []
    for (var i = 11; i >= 0; i--) {
      var d = new Date;
      d = new Date(d.setMonth(d.getMonth() - i));
      var month = d.getMonth();
      var year = d.getFullYear();
      startDate = new Date(year, month, 1);
      var endDate = new Date(year, month + 1, 0, 23, 59, 59, 999);
      var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
      ];
      monthName = monthNames[month];
      barData.push({
        month: monthName,
        startDate: startDate,
        endDate: endDate
      });
    }

    async.forEach(barData, function (item, eachCallback) {
      async.parallel([
        function (callback) {
          Invoices.count({
            where: {
              companyId: req.companyId,
              createdAt: { $between: [item.startDate, item.endDate] }
            }
          })
            .then(function (totalInvoicesCount) {
              item.total = totalInvoicesCount;
              callback();
            })
            .catch(function (err) {
              console.log(err)
              callback('Oops, Something went wrong ER921010011');
            });
        },
        function (callback) {

          Invoices.count({
            where: {
              status: 'RECEIVED',
              companyId: req.companyId,
              createdAt: { $between: [item.startDate, item.endDate] }
            }
          })
            .then(function (receivedInvoicesCount) {
              item.received = receivedInvoicesCount;
              callback();
            })
            .catch(function (err) {
              console.log(err)
              callback('Oops, Something went wrong ER921010012');
            });
        }
      ], function (err) {
        if (err) {
          eachCallback(err);
        }
        else {
          eachCallback();
        }
      });
    }, function (err) {
      if (err) {
        res.statusCode = 500;
        res.setHeader('response-description', err);
        res.end();
      }
      else {
        var data = {};
        data.barData12 = [];
        data.barData12 = barData;
        data.barData6 = barData.slice(9);
        res.json(data);
      }
    })
  });


  app.get('/api/dashboard/summary', gatekeeper.authenticateUser, function (req, res, next) {

    var invoices, vehicles, tasks;

    async.parallel([
      function (callback) {
        Invoices.count({
          where: {
            status: 'PENDING',
            companyId: req.companyId
          }
        })
          .then(function (totalInvoicesCount) {
            invoices = totalInvoicesCount;
            callback();
          })
          .catch(function (err) {
            console.log(err)
            callback('Oops, Something went wrong ER921010011');
          });
      },
      function (callback) {

        Tasks.count({
          where: {
            status: 'PENDING',
            companyId: req.companyId,
          }
        })
          .then(function (tasksCount) {
            tasks = tasksCount;
            console.log(tasks)
            callback();
          })
          .catch(function (err) {
            console.log(err)
            callback('Oops, Something went wrong ER921010012');
          });
      },
      function (callback) {

        Vehicles.count({
          where: {
            status: 'AVAILABLE',
            companyId: req.companyId,
          }
        }).then(function (vehiclesCount) {
            vehicles = vehiclesCount;
            callback();
          })
          .catch(function (err) {
            console.log(err)
            callback('Oops, Something went wrong ER921010013');
          });
      }
    ], function (err) {
      if (err) {
        res.statusCode = 500;
        res.setHeader('response-description', err);
        res.end();
      }
      else {
        var summary = {};
        summary.vehicles = vehicles;
        summary.invoices = invoices;
        summary.tasks = tasks;
        res.json(summary);
      }
    });
  });

}