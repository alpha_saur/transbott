const Vehicles = require('../models/vehicles');
const crypto = require('crypto');
const random = require('../utilities/random');
const utils = require('../utilities/utils');
const gatekeeper = require('../middlewares/gatekeeper');

module.exports = function (app) {

  app.get('/api/vehicles', gatekeeper.authenticateUser, function (req, res, next) {
    Vehicles.findAll({
      where: {
        companyId: req.companyId
      },
      order: '"numberPlate" ASC'      
    })
      .then(function (VehiclesFromRepo) {
        res.json(VehiclesFromRepo);
        res.end();
      })
      .catch(function () {
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9212357');
        res.end();
      });
  });

  app.get('/api/vehicles/available', gatekeeper.authenticateUser, function (req, res, next) {
    Vehicles.findAll({
      where: {
        status: 'AVAILABLE',
        companyId: req.companyId
      },
      order: '"numberPlate" DESC'
    })
      .then(function (VehiclesFromRepo) {
        res.json(VehiclesFromRepo);
        res.end();
      })
      .catch(function () {
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9212357');
        res.end();
      });
  });

  app.get('/api/vehicles/:numberPlate', gatekeeper.authenticateUser, function (req, res, next) {
    Vehicles.findOne({
      where: {
        numberPlate: req.params.numberPlate,
        companyId: req.companyId
      }
    })
      .then(function (vehicleFromRepo) {
        if (vehicleFromRepo) {
          res.json(vehicleFromRepo);
          res.end();
        }
        else {
          res.statusCode = 400;
          res.setHeader('response-description', 'Invalid vehicle id, vehicle not found');
          res.end();
        }
      })
      .catch(function () {
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9212358');
        res.end();
      });
  });

  app.post('/api/vehicles', gatekeeper.authenticateUser, function (req, res, next) {
    utils.velidateRequiredKeys(req.body,
      [
        { key: 'numberPlate', name: 'Number plate' },
        { key: 'ownerType', name: 'Owner type' },
        { key: 'vehicleType', name: 'Vehicle type' },
        { key: 'mulkiaExpiry', name: 'Mulkia' },
        { key: 'mulkia', name: 'Mulkia Expiry' },
        { key: 'owner', name: 'Owner' }
      ],
      function (errorField) {
        if (!errorField) {

          Vehicles.findOne({
            where: {
              numberPlate: req.body.numberPlate,
              companyId: req.companyId
            }
          })
            .then(function (vehicleFromRepo) {

              if (!vehicleFromRepo) {
                Vehicles.create({
                  numberPlate: req.body.numberPlate.toUpperCase(),
                  vehicleType: req.body.vehicleType.toUpperCase(),
                  ownerType: req.body.ownerType.toUpperCase(),
                  mulkiaExpiry: req.body.mulkiaExpiry,
                  mulkia: req.body.mulkia,
                  owner: req.body.owner,
                  companyId: req.companyId,
                  companyName: req.companyName
                })
                  .then(function () {
                    res.statusCode = 201;
                    res.end();
                  })
                  .catch(function () {
                    res.statusCode = 500;
                    res.setHeader('response-description', 'Oops, Something went wrong ER9212345');
                    res.end();
                  });
              }
              else {
                res.statusCode = 403;
                res.setHeader('response-description', 'Number plate already exists');
                res.end(); ER9212344
              }
            })
            .catch(function (err) {
              console.log(err)
              res.statusCode = 500;
              res.setHeader('response-description', 'Oops, Something went wrong ');
              res.end();
            });


        }
        else {
          res.statusCode = 400;
          res.setHeader('response-description', errorField + ' is required');
          res.end();
        }
      })
  });

  app.put('/api/vehicles', gatekeeper.authenticateUser, function (req, res, next) {
    utils.velidateRequiredKeys(req.body,
      [
        { key: 'ownerType', name: 'Owner type' },
        { key: 'mulkiaExpiry', name: 'Mulkia' },
        { key: 'mulkia', name: 'Mulkia Expiry' },
        { key: 'owner', name: 'Owner' }
      ],
      function (errorField) {
        if (!errorField) {

          Vehicles.findOne({
            where: {
              numberPlate: req.body.numberPlate.toUpperCase(),
              companyId: req.companyId
            }
          })
            .then(function (vehicleFromRepo) {

              if (vehicleFromRepo) {

                vehicleFromRepo.ownerType = req.body.ownerType.toUpperCase();
                vehicleFromRepo.mulkiaExpiry = req.body.mulkiaExpiry;
                vehicleFromRepo.mulkia = req.body.mulkia;
                vehicleFromRepo.owner = req.body.owner;

                vehicleFromRepo.save()
                  .then(function () {
                    res.statusCode = 200;
                    res.end();
                  })
                  .catch(function () {
                    res.statusCode = 500;
                    res.setHeader('response-description', 'Oops, Something went wrong ER9212349');
                    res.end();
                  });


              }
              else {
                res.statusCode = 400;
                res.setHeader('response-description', 'Vehicle not found');
                res.end();
              }
            })
            .catch(function (err) {
              console.log(err)
              res.statusCode = 500;
              res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
              res.end();
            });


        }
        else {
          res.statusCode = 400;
          res.setHeader('response-description', errorField + ' is required');
          res.end();
        }
      });
  });

  app.put('/api/vehicles/available', gatekeeper.authenticateUser, function (req, res, next) {
    utils.velidateRequiredKeys(req.body,
      [
        { key: 'numberPlate', name: 'Number plate' }
      ],
      function (errorField) {
        if (!errorField) {

          Vehicles.findOne({
            where: {
              numberPlate: req.body.numberPlate.toUpperCase(),
              companyId: req.companyId
            }
          })
            .then(function (vehicleFromRepo) {

              if (vehicleFromRepo) {

                vehicleFromRepo.status = 'AVAILABLE';

                vehicleFromRepo.save()
                  .then(function () {
                    res.statusCode = 200;
                    res.end();
                  })
                  .catch(function () {
                    res.statusCode = 500;
                    res.setHeader('response-description', 'Oops, Something went wrong ER9212349');
                    res.end();
                  });


              }
              else {
                res.statusCode = 400;
                res.setHeader('response-description', 'Vehicle not found');
                res.end();
              }
            })
            .catch(function (err) {
              console.log(err)
              res.statusCode = 500;
              res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
              res.end();
            });


        }
        else {
          res.statusCode = 400;
          res.setHeader('response-description', errorField + ' is required');
          res.end();
        }
      })
  });

}
