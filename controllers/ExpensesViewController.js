const crypto = require('crypto');
const random = require('../utilities/random');
const utils = require('../utilities/utils');
const gatekeeper = require('../middlewares/gatekeeper');
const apiHelpers = require('../utilities/apiHelpers');
const async = require('async');

module.exports = function (app) {


  app.post('/expenses', gatekeeper.authenticateSession, function (req, res, next) {

    apiHelpers.genericAPIHelperWithAuth(req, '/api/expenses', 'POST',
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 201) {
          req.flash('success', 'Expenses successfully added');
          req.session.save(function () {
            res.redirect('/tasks/' + req.body.numberPlate + '/' + req.body.taskId);
          });
        }
        else {
          req.flash('error', apiRepo.description);
          req.session.save(function () {
            res.redirect('/tasks/' + req.body.numberPlate + '/' + req.body.taskId);
          });
        }
      });

  });

  app.post('/expenses/invoices', gatekeeper.authenticateSession, function (req, res, next) {

    apiHelpers.genericAPIHelperWithAuth(req, '/api/expenses/invoices', 'POST',
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 201) {
          req.flash('success', 'Expenses successfully added');
          req.session.save(function () {
            res.redirect('/invoices/details/' + req.body.taskId + '/' + req.body.invoiceId);
          });
        }
        else {
          req.flash('error', apiRepo.description);
          req.session.save(function () {
            res.redirect('/invoices/details/' + req.body.taskId + '/' + req.body.invoiceId);
          });
        }
      });

  });


}
