const crypto = require('crypto');
const random = require('../utilities/random');
const utils = require('../utilities/utils');
const gatekeeper = require('../middlewares/gatekeeper');
const apiHelpers = require('../utilities/apiHelpers');
const async = require('async');

module.exports = function (app) {

  app.get('/invoices/:taskId', gatekeeper.authenticateSession, function (req, res, next) {
    var invoices = [];
    var task = {};
    async.parallel([
      function (callback) {
        apiHelpers.genericAPIHelperWithAuth(req, '/api/tasks/details/' + req.params.taskId, 'GET',
          req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
            if (apiRepo.statusCode == 200) {
              task = apiRepo.response;
              callback();
            }
            else {
              callback(apiHelpers.description);
            }
          });
      },
      function (callback) {
        apiHelpers.genericAPIHelperWithAuth(req, '/api/invoices/' + req.params.taskId, 'GET',
          req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
            if (apiRepo.statusCode == 200) {
              invoices = apiRepo.response;
              callback();
            }
            else {
              callback(apiHelpers.description);
            }
          });
      }
    ], function (err) {
      if (!err) {
        res.render('invoices/invoices',
          {
            error: req.flash('error')[0],
            success: req.flash('success')[0],
            task: task,
            invoices: invoices,
            session: req.session
          });
      }
      else {
        req.flash('error', err);
        req.session.save(function () {
          res.redirect('/');
        });
      }
    });
  });


  app.get('/invoices', gatekeeper.authenticateSession, function (req, res, next) {
    res.render('invoices/invoices-json',
      {
        error: req.flash('error')[0],
        success: req.flash('success')[0],
        session: req.session
      });
  });

  app.get('/invoices/details/:taskId/:invoiceId', gatekeeper.authenticateSession, function (req, res, next) {
    apiHelpers.genericAPIHelperWithAuth(req, '/api/invoices/details/' + req.params.invoiceId, 'GET',
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 200) {
          res.render('invoices/invoice-details',
            {
              error: req.flash('error')[0],
              success: req.flash('success')[0],
              invoice: apiRepo.response,
              session: req.session
            });
        }
        else {
          req.flash('error', apiRepo.description);
          req.session.save(function () {
            res.redirect('/invoices/' + req.params.taskId);
          });
        }
      });

  });

  app.post('/invoices', gatekeeper.authenticateSession, function (req, res, next) {

    apiHelpers.genericAPIMultiPartHelperWithAuth(req, '/api/invoices', 'POST',
      ['timesheetPhoto'],
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 201) {
          req.flash('success', 'Invoice successfully created');
          req.session.save(function () {
            res.redirect('/invoices/details/' + apiRepo.response);
          });
        }
        else {
          req.flash('error', apiRepo.description);
          req.session.save(function () {
            res.redirect('/invoices/' + req.body.taskId);
          });
        }
      });

  });

  app.post('/invoices/received', gatekeeper.authenticateSession, function (req, res, next) {
    apiHelpers.genericAPIHelperWithAuth(req, '/api/invoices/received', 'PUT',
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 200) {
          req.flash('success', 'Invoice successfully marked as received');
          req.session.save(function () {
            res.redirect('/invoices/details/' + req.body.taskId + '/' + req.body.invoiceId);
          });
        }
        else {
          req.flash('error', apiRepo.description);
          req.session.save(function () {
            res.redirect('/invoices/details/' + req.body.taskId + '/' + req.body.invoiceId);
          });
        }
      });

  });

  app.get('/json/invoices/data', gatekeeper.authenticateSession, function (req, res, next) {
    apiHelpers.genericAPIHelperWithAuth(req, '/api/json/invoices?search=' + encodeURIComponent(req.query.search) + '&offset=' + encodeURIComponent(req.query.offset) + '&limit=' + encodeURIComponent(req.query.limit), 'GET',
      req.session.transbott_userName + ':' + req.session.transbott_password, function (apiRepo) {
        if (apiRepo.statusCode == 200) {
          res.json({ rows: apiRepo.response.rows, total: apiRepo.response.total });
          res.end();
        }
        else {
          req.flash('error', apiRepo.description);
          res.json({ rows: [], total: 0 });
          res.end();
        }
      });

  });

}
