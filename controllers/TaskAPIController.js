const Tasks = require('../models/tasks');
const Vehicle = require('../models/vehicles');
const crypto = require('crypto');
const random = require('../utilities/random');
const utils = require('../utilities/utils');
const config = require('../config/config');
var fs = require('fs');
const gatekeeper = require('../middlewares/gatekeeper');

module.exports = function (app) {

  app.get('/api/tasks/:numberPlate', gatekeeper.authenticateUser, function (req, res, next) {
    Tasks.findAll({
      where: {
        numberPlate: req.params.numberPlate,
        companyId: req.companyId
      },
      order: '"createdAt" DESC'
    })
      .then(function (taskFromRepo) {
        res.json(taskFromRepo);
        res.end();
      })
      .catch(function () {
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
        res.end();
      });
  });


  app.get('/api/tasks', gatekeeper.authenticateUser, function (req, res, next) {
    Tasks.findAll({
      where: {
        companyId: req.companyId
      },
      order: '"createdAt" DESC'
    })
      .then(function (taskFromRepo) {
        res.json(taskFromRepo);
        res.end();
      })
      .catch(function () {
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
        res.end();
      });
  });

  app.get('/api/tasks/details/:taskId', gatekeeper.authenticateUser, function (req, res, next) {
    Tasks.findOne({
      where: {
        taskId: req.params.taskId,
        companyId: req.companyId
      }
    })
      .then(function (taskFromRepo) {
        res.json(taskFromRepo);
        res.end();
      })
      .catch(function () {
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
        res.end();
      });
  });

  app.post('/api/tasks', gatekeeper.authenticateUser, function (req, res, next) {
    utils.velidateRequiredKeys(req.body,
      [
        { key: 'numberPlate', name: 'Number plate' },
        { key: 'hiredBy', name: 'Hiring Company name' },
        { key: 'taskTitle', name: 'Task title' },
        { key: 'workingRate', name: 'Working rate' },
        { key: 'intervalType', name: 'Hired for interval' },
        { key: 'withDiesel', name: 'With or Without diesel' },
        { key: 'startDate', name: 'Date' }
      ],
      function (errorField) {
        if (!errorField) {
          if (req.body.intervalType != 'notsure' && !req.body.interval) {
            res.statusCode = 400;
            res.setHeader('response-description', 'Number of ' + req.body.intervalType + ' are required');
            res.end();
          }
          else {

            var taskDate = new Date(req.body.startDate);
            var endDate = new Date(taskDate);
            var interval = Number(req.body.interval);
            var floorInterval = Math.floor(interval);
            var workingHours = req.body.interval ? req.body.interval : 0;
            if (req.body.intervalType == 'days') {
              endDate = new Date(endDate.setDate(taskDate.getDate() + req.body.interval));
              workingHours = Number(req.body.interval) * config.hoursPerDay;
            }
            else if (req.body.intervalType == 'months') {
              endDate = new Date(endDate.setMonth(taskDate.getMonth() + floorInterval));
              if (interval - floorInterval > 0) {
                endDate = new Date(endDate.getDate(taskDate.getDate() + 15));
              }
              var days = utils.getDaysFromTwoDates(taskDate, endDate);
              workingHours = Number(req.body.interval) * config.hoursPerMonth;
            }
            else if (req.body.intervalType == 'hours') {
              endDate = new Date(endDate.setHours(taskDate.getHours() + floorInterval));
              if (interval - floorInterval > 0) {
                endDate = new Date(endDate.setMinutes(taskDate.getHours() + 30));
                workingHours = Number(req.body.interval);
              }

            }
            else {
              workingHours = 0;
              endDate = null;
            }

            var total = workingHours * Number(req.body.workingRate);


            Tasks.create({
              taskId: 5 + '' + random.random10Digits(),
              numberPlate: req.body.numberPlate.toUpperCase(),
              hiredBy: req.body.hiredBy,
              taskTitle: req.body.taskTitle,
              startDate: req.body.startDate,
              taskDate: taskDate,
              nextInvoiceStartDate: taskDate,
              workingHours: workingHours,
              remainingHours: workingHours,
              startTime: req.body.startTime,
              withDiesel: req.body.withDiesel,
              workingRate: req.body.workingRate,
              interval: req.body.interval,
              intervalType: req.body.intervalType,
              total: total,
              income: total,
              endDate: endDate,
              totalExpenses: 0,
              taskDescription: req.body.taskDescription,
              createdBy: req.userName,
              companyId: req.companyId,
              companyName: req.companyName
            })
              .then(function () {
                Vehicle.findOne({
                  where: {
                    numberPlate: req.body.numberPlate,
                    companyId: req.companyId
                  }
                })
                  .then(function (vehicleFromRepo) {
                    vehicleFromRepo.status = 'HIRED';
                    vehicleFromRepo.save()
                      .then(function () {
                        res.statusCode = 201;
                        res.end();
                      })
                      .catch(function (err) {
                        console.log(err);
                        res.statusCode = 500;
                        res.setHeader('response-description', 'Oops, Something went wrong ER9210108');
                        res.end();
                      });
                  })
                  .catch(function (err) {
                    console.log(err);
                    res.statusCode = 500;
                    res.setHeader('response-description', 'Oops, Something went wrong ER9210108');
                    res.end();
                  });
              })
              .catch(function (err) {
                console.log(err);
                res.statusCode = 500;
                res.setHeader('response-description', 'Oops, Something went wrong ER9210108');
                res.end();
              });
          }
        }
        else {
          res.statusCode = 400;
          res.setHeader('response-description', errorField + ' is required');
          res.end();
        }
      })
  });


  app.put('/api/tasks/addhours', gatekeeper.authenticateUser, function (req, res, next) {

    utils.velidateRequiredKeys(req.body,
      [
        { key: 'hours', name: 'Number of Hours' }
      ],
      function (errorField) {
        if (!errorField) {
          Tasks.findOne({
            where: {
              taskId: req.body.taskId,
              status: 'PENDING',
              companyId: req.companyId
            }
          })
            .then(function (taskFromRepo) {

              if (taskFromRepo) {

                taskFromRepo.workingHours = Number(req.body.hours) + Number(taskFromRepo.workingHours);
                taskFromRepo.remainingHours = Number(req.body.hours) + Number(taskFromRepo.remainingHours);
                taskFromRepo.total = (Number(req.body.hours) * Number(taskFromRepo.workingRate)) + Number(taskFromRepo.total);
                taskFromRepo.income = (Number(req.body.hours) * Number(taskFromRepo.workingRate)) + Number(taskFromRepo.income);
                taskFromRepo.save()
                  .then(function () {

                    res.statusCode = 200;
                    res.end();

                  })
                  .catch(function (err) {
                    console.log(err)
                    res.statusCode = 500;
                    res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
                    res.end();
                  });
              }
              else {
                res.statusCode = 400;
                res.setHeader('response-description', 'Task not found');
                res.end();
              }
            })
            .catch(function (err) {
              console.log(err)
              res.statusCode = 500;
              res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
              res.end();
            });

        }
        else {
          res.statusCode = 400;
          res.setHeader('response-description', errorField + ' is required');
          res.end();
        }
      })

  });

  app.put('/api/tasks/received', gatekeeper.authenticateUser, function (req, res, next) {
    if (req.documentsRequired == 'YES' && !req.files.taskPhoto) {
      res.statusCode = 400;
      res.setHeader('response-description', 'Task copy is required');
      res.end();
    }
    else {
      utils.velidateRequiredKeys(req.body,
        [
          { key: 'paymentMethod', name: 'Payment method' }
        ],
        function (errorField) {
          if (!errorField) {
            if (req.body.paymentMethod.toUpperCase() == 'CHEQUE' && !req.body.chequeNumber) {
              res.statusCode = 400;
              res.setHeader('response-description', 'Cheque number is required');
              res.end();
            }
            else {

              Tasks.findOne({
                where: {
                  taskId: req.body.taskId,
                  status: 'COMPLETE',
                  companyId: req.companyId
                }
              })
                .then(function (taskFromRepo) {

                  if (taskFromRepo) {

                    if (taskFromRepo.lpo) {
                      taskFromRepo.markedAsReceivedBy = req.userName;
                      taskFromRepo.paymentMethod = req.body.paymentMethod;
                      taskFromRepo.paymentDescription = req.body.paymentDescription;
                      if (taskFromRepo.paymentMethod == 'CHEQUE') {
                        taskFromRepo.chequeNumber = req.body.chequeNumber;
                      }
                      taskFromRepo.discount = req.body.discount ? Number(req.body.discount) : 0;
                      taskFromRepo.totalExpenses = taskFromRepo.totalExpenses ? Number(taskFromRepo.totalExpenses) : 0;
                      taskFromRepo.income = Number(taskFromRepo.total) - taskFromRepo.totalExpenses - taskFromRepo.discount;
                      taskFromRepo.status = 'RECEIVED';

                      utils.saveFileAt('/files/tasks/' + req.body.taskId, req.files.taskPhoto, function (err, fileName) {
                        if (err && err != 'nofile') {
                          console.log(err);
                          res.statusCode = 500;
                          res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
                          res.end();
                        }
                        else {
                          var docs = [];
                          if (req.files.taskPhoto) {
                            var preDocTask = JSON.parse(JSON.stringify(taskFromRepo));
                            var oldDocs = preDocTask.taskDocuments ? preDocTask.taskDocuments : [];
                            for (var i = 0; i < oldDocs.length; i++) {
                              if (oldDocs.fileType !== 'task') {
                                docs.push(oldDocs[i]);
                              }
                            }
                            req.files.taskPhoto.fileType = 'task';
                            req.files.taskPhoto.fileName = fileName;
                            req.files.taskPhoto.uploadedBy = req.userName;
                            docs.push(req.files.taskPhoto)

                            taskFromRepo.taskDocuments = docs;
                          }
                          taskFromRepo.save()
                            .then(function () {

                              res.statusCode = 200;
                              res.end();

                            })
                            .catch(function (err) {
                              console.log(err)
                              res.statusCode = 500;
                              res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
                              res.end();
                            });
                        }
                      })
                    }
                    else {
                      res.statusCode = 400;
                      res.setHeader('response-description', 'Please provide LPO before marking as received');
                      res.end();
                    }
                  }
                  else {
                    res.statusCode = 400;
                    res.setHeader('response-description', 'Task not found');
                    res.end();
                  }
                })
                .catch(function (err) {
                  console.log(err)
                  res.statusCode = 500;
                  res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
                  res.end();
                });
            }
          }
          else {
            res.statusCode = 400;
            res.setHeader('response-description', errorField + ' is required');
            res.end();
          }
        })
    }
  });

  app.put('/notbeingusedfornow/api/tasks/complete', gatekeeper.authenticateUser, function (req, res, next) {
    if (req.documentsRequired == 'YES' && !req.files.timesheetPhoto) {
      res.statusCode = 400;
      res.setHeader('response-description', 'Time sheet copy is required');
      res.end();
    }
    else {
      utils.velidateRequiredKeys(req.body,
        [
          { key: 'workingHours', name: 'Working hours' }
        ],
        function (errorField) {
          if (!errorField) {

            Tasks.findOne({
              where: {
                taskId: req.body.taskId,
                status: 'PENDING',
                companyId: req.companyId
              }
            })
              .then(function (taskFromRepo) {

                if (taskFromRepo) {

                  taskFromRepo.workingHours = req.body.workingHours;
                  taskFromRepo.status = 'COMPLETE';
                  taskFromRepo.total = Number(req.body.workingHours) * Number(taskFromRepo.workingRate);
                  taskFromRepo.income = taskFromRepo.total;
                  taskFromRepo.income = taskFromRepo.total - taskFromRepo.totalExpenses;

                  utils.saveFileAt('/files/tasks/' + req.body.taskId, req.files.timesheetPhoto, function (err, fileName) {
                    if (err && err != 'nofile') {
                      console.log(err);
                      res.statusCode = 500;
                      res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
                      res.end();
                    }
                    else {
                      var docs = [];
                      if (req.files.timesheetPhoto) {
                        var oldDocs = taskFromRepo.taskDocuments ? taskFromRepo.taskDocuments : [];
                        for (var i = 0; i < oldDocs.length; i++) {
                          if (oldDocs.fileType !== 'timesheet') {
                            docs.push(oldDocs[i]);
                          }
                        }
                        req.files.timesheetPhoto.fileType = 'timesheet';
                        req.files.timesheetPhoto.fileName = fileName;
                        req.files.timesheetPhoto.uploadedBy = req.userName;
                        docs.push(req.files.timesheetPhoto)

                        taskFromRepo.taskDocuments = docs;
                      }
                      taskFromRepo.save()
                        .then(function () {

                          Vehicle.findOne({
                            where: {
                              numberPlate: taskFromRepo.numberPlate,
                              status: 'HIRED',
                              companyId: req.companyId
                            }
                          })
                            .then(function (vehicleFromRepo) {

                              if (vehicleFromRepo) {

                                vehicleFromRepo.status = 'AVAILABLE';

                                vehicleFromRepo.save()
                                  .then(function () {
                                    res.statusCode = 200;
                                    res.end();
                                  })
                                  .catch(function (err) {
                                    console.log(err)
                                    res.statusCode = 500;
                                    res.setHeader('response-description', 'Oops, Something went wrong ER92123492');
                                    res.end();
                                  });

                              }
                              else {
                                res.statusCode = 400;
                                res.setHeader('response-description', 'Vehicle not found');
                                res.end();
                              }

                            })
                            .catch(function (err) {
                              console.log(err)
                              res.statusCode = 500;
                              res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
                              res.end();
                            });

                        })
                        .catch(function (err) {
                          console.log(err)
                          res.statusCode = 500;
                          res.setHeader('response-description', 'Oops, Something went wrong ER92123491');
                          res.end();
                        });

                    }

                  });


                }
                else {
                  res.statusCode = 400;
                  res.setHeader('response-description', 'Task not found');
                  res.end();
                }
              })
              .catch(function (err) {
                console.log(err)
                res.statusCode = 500;
                res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
                res.end();
              });

          }
          else {
            res.statusCode = 400;
            res.setHeader('response-description', errorField + ' is required');
            res.end();
          }
        })
    }
  });

  app.put('/api/tasks/lpo', gatekeeper.authenticateUser, function (req, res, next) {
    if (req.documentsRequired == 'YES' && !req.files.lpoPhoto) {
      res.statusCode = 400;
      res.setHeader('response-description', 'LPO copy is required');
      res.end();
    }
    else {
      utils.velidateRequiredKeys(req.body,
        [
          { key: 'lpo', name: 'LPO Number' }
        ],
        function (errorField) {
          if (!errorField) {

            Tasks.findOne({
              where: {
                taskId: req.body.taskId,
                status: { $ne: 'RECEIVED' },
                companyId: req.companyId
              }
            })
              .then(function (taskFromRepo) {

                if (taskFromRepo) {

                  taskFromRepo.lpo = req.body.lpo;

                  utils.saveFileAt('/files/tasks/' + req.body.taskId, req.files.lpoPhoto, function (err, fileName) {
                    if (err && err != 'nofile') {
                      console.log(err);
                      res.statusCode = 500;
                      res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
                      res.end();
                    }
                    else {
                      var docs = [];
                      if (req.files.lpoPhoto) {
                        var oldDocs = taskFromRepo.taskDocuments ? taskFromRepo.taskDocuments : [];
                        for (var i = 0; i < oldDocs.length; i++) {
                          if (oldDocs.fileType !== 'lpo') {
                            docs.push(oldDocs[i]);
                          }
                        }
                        req.files.lpoPhoto.fileType = 'lpo';
                        req.files.lpoPhoto.fileName = fileName;
                        req.files.lpoPhoto.uploadedBy = req.userName;
                        docs.push(req.files.lpoPhoto)

                        taskFromRepo.taskDocuments = docs;
                      }
                      taskFromRepo.save()
                        .then(function () {

                          res.statusCode = 200;
                          res.end();

                        })
                        .catch(function (err) {
                          console.log(err)
                          res.statusCode = 500;
                          res.setHeader('response-description', 'Oops, Something went wrong ER92123491');
                          res.end();
                        });

                    }

                  });


                }
                else {
                  res.statusCode = 400;
                  res.setHeader('response-description', 'Task not found');
                  res.end();
                }
              })
              .catch(function (err) {
                console.log(err)
                res.statusCode = 500;
                res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
                res.end();
              });

          }
          else {
            res.statusCode = 400;
            res.setHeader('response-description', errorField + ' is required');
            res.end();
          }
        })
    }
  });


  app.get('/api/json/tasks', gatekeeper.authenticateUser, function (req, res, next) {

    var limit = 10;
    var offset = 0;
    var search = '';

    if (req.query.limit != null && req.query.limit != 'undefined') {
      limit = req.query.limit;
    }

    if (req.query.offset != null && req.query.offset != 'undefined') {
      offset = req.query.offset;
    }

    if (req.query.search && req.query.search != 'undefined') {
      search = req.query.search;
    }



    var searchnumber;

    searchnumber = Number(search)

    if (!searchnumber) {
      searchnumber = null
    }

    Tasks.count({
      where: {
        companyId: req.companyId,
        $or: [
          { status: { $iLike: '%' + search + '%' } },
          { taskTitle: { $iLike: '%' + search + '%' } },
          { workingHours: searchnumber },
          { workingRate: searchnumber },
          { income: searchnumber },
          { numberPlate: { $iLike: '%' + search + '%' } },
          { startDate: { $iLike: '%' + search + '%' } }
        ]
      }
    })
      .then(function (totalCount) {
        Tasks.findAll({
          where: {
            companyId: req.companyId,
            $or: [
              { status: { $iLike: '%' + search + '%' } },
              { taskTitle: { $iLike: '%' + search + '%' } },
              { workingHours: searchnumber },
              { workingRate: searchnumber },
              { income: searchnumber },
              { numberPlate: { $iLike: '%' + search + '%' } },
              { startDate: { $iLike: '%' + search + '%' } }
            ]
          },
          order: '"createdAt" DESC',
          offset: offset,
          limit: limit
        })
          .then(function (taskFromRepo) {
            res.json({ rows: taskFromRepo, total: totalCount });
            res.end();
          })
          .catch(function (err) {
            console.log(err);
            res.statusCode = 500;
            res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
            res.end();
          });
      })
      .catch(function (err) {
        console.log(err);
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
        res.end();
      });
  });

  app.get('/api/json/tasks/:numberPlate', gatekeeper.authenticateUser, function (req, res, next) {

    var limit = 10;
    var offset = 0;
    var search = '';

    if (req.query.limit != null && req.query.limit != 'undefined') {
      limit = req.query.limit;
    }

    if (req.query.offset != null && req.query.offset != 'undefined') {
      offset = req.query.offset;
    }

    if (req.query.search && req.query.search != 'undefined') {
      search = req.query.search;
    }



    var searchnumber;

    searchnumber = Number(search)

    if (!searchnumber) {
      searchnumber = null
    }

    Tasks.count({
      where: {
        companyId: req.companyId,
        numberPlate: req.params.numberPlate,
        $or: [
          { taskTitle: { $iLike: '%' + search + '%' } },
          { workingHours: searchnumber },
          { workingRate: searchnumber },
          { income: searchnumber },
          { numberPlate: { $iLike: '%' + search + '%' } },
          { status: { $iLike: '%' + search + '%' } },
          { startDate: { $iLike: '%' + search + '%' } }
        ]
      }
    })
      .then(function (totalCount) {
        Tasks.findAll({
          where: {
            companyId: req.companyId,
            numberPlate: req.params.numberPlate,
            $or: [
              { taskTitle: { $iLike: '%' + search + '%' } },
              { workingHours: searchnumber },
              { workingRate: searchnumber },
              { income: searchnumber },
              { numberPlate: { $iLike: '%' + search + '%' } },
              { status: { $iLike: '%' + search + '%' } },
              { startDate: { $iLike: '%' + search + '%' } }
            ]
          },
          order: '"createdAt" DESC',
          offset: offset,
          limit: limit
        })
          .then(function (taskFromRepo) {
            res.json({ rows: taskFromRepo, total: totalCount });
            res.end();
          })
          .catch(function (err) {
            console.log(err);
            res.statusCode = 500;
            res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
            res.end();
          });
      })
      .catch(function (err) {
        console.log(err);
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9210100');
        res.end();
      });
  });

  app.put('/api/tasks/complete', gatekeeper.authenticateUser, function (req, res, next) {


    Tasks.findOne({
      where: {
        taskId: req.body.taskId,
        status: 'PENDING',
        companyId: req.companyId
      }
    })
      .then(function (taskFromRepo) {

        if (taskFromRepo) {
          taskFromRepo.status = 'COMPLETE';

          taskFromRepo.save()
            .then(function () {

              Vehicle.findOne({
                where: {
                  numberPlate: taskFromRepo.numberPlate,
                  status: 'HIRED',
                  companyId: req.companyId
                }
              })
                .then(function (vehicleFromRepo) {

                  if (vehicleFromRepo) {

                    vehicleFromRepo.status = 'AVAILABLE';

                    vehicleFromRepo.save()
                      .then(function () {
                        res.statusCode = 200;
                        res.end();
                      })
                      .catch(function (err) {
                        console.log(err)
                        res.statusCode = 500;
                        res.setHeader('response-description', 'Oops, Something went wrong ER92123492');
                        res.end();
                      });

                  }
                  else {
                    res.statusCode = 400;
                    res.setHeader('response-description', 'Vehicle not found');
                    res.end();
                  }

                })
                .catch(function (err) {
                  console.log(err)
                  res.statusCode = 500;
                  res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
                  res.end();
                });

            })
            .catch(function (err) {
              console.log(err)
              res.statusCode = 500;
              res.setHeader('response-description', 'Oops, Something went wrong ER92123491');
              res.end();
            });


        }
        else {
          res.statusCode = 400;
          res.setHeader('response-description', 'Task not found');
          res.end();
        }
      })
      .catch(function (err) {
        console.log(err)
        res.statusCode = 500;
        res.setHeader('response-description', 'Oops, Something went wrong ER9212350');
        res.end();
      });


  });
}