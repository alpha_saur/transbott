const crypto = require('crypto');
const random = require('../utilities/random');
const utils = require('../utilities/utils');
const gatekeeper = require('../middlewares/gatekeeper');
const apiHelpers = require('../utilities/apiHelpers');

module.exports = function (app) {


  app.post('/downloads/excels/tasks', gatekeeper.authenticateSession, function (req, res, next) {
    var dates = '?startDate='+encodeURIComponent(req.body.startDate)+'&endDate='+encodeURIComponent(req.body.endDate);
    res.redirect('/api/downloads/excels/tasks/' + req.session.company.companyId+dates);
  });

    app.post('/downloads/excels/vehicles/tasks', gatekeeper.authenticateSession, function (req, res, next) {
    var dates = '?startDate='+encodeURIComponent(req.body.startDate)+'&endDate='+encodeURIComponent(req.body.endDate);
    res.redirect('/api/downloads/excels/tasks/' + req.session.company.companyId+ '/' + req.body.numberPlate+dates);
  });

      app.post('/downloads/excels/invoices', gatekeeper.authenticateSession, function (req, res, next) {
    var dates = '?startDate='+encodeURIComponent(req.body.startDate)+'&endDate='+encodeURIComponent(req.body.endDate);
    res.redirect('/api/downloads/excels/invoices/' + req.session.company.companyId+dates);
  });


}
